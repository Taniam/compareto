/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenamientocadenas;

/**
 *
 * @author Capeleishon, Esthereishon, Tanieshon
 */
public class OrdenamientoCadenas {
    String[] alumnos;
    int m;//cuantos tengo ya
    int n; //hasta cuantos puedo tener
    
    OrdenamientoCadenas(){
        n=72;
        m=0;//Todavía no tengo ningún inscrito
        alumnos=new String [n];
    }
    OrdenamientoCadenas(String[] alumnos){ //Le voy a pasar un arreglo de alumnos
        this.alumnos=alumnos;
        m=alumnos.length;
    }
    void setM(int m){//Le asigna un valor específico al campo M
        this.m=m;
    }
    void setN(int n){//Le asigna un valor específico al campo N
        this.n=n;
        n=n;
    }
    int getM(){
        return(m);
    }
    int getN(){
        return(this.n);
    }
    String getAlumno(int i){
        return(this.alumnos[i]);
    }
    /**
     * 
     * @param cadena es la cadena a insertar en la posición 
     *               del arreglo alumnos 
     * @param i es la posición de cadena en el arreglo alumno
     *
     * Esto es un setter para asignar una cadena a una posición específica
     * en el arreglo de cadenas alumnos
     */
    void setCadena(String cadena, int i){
        this.alumnos[i]=cadena;
    }
    int pos(String cadena){
        int resultado;
        int j=0; //Es la posicion de cadenas en alumno
        while(getAlumno(j).compareTo(cadena)>0)j++;
        resultado=j;
        return resultado;        
    }         
     /**
     * 
     * @param t 
     */
    void setOC(String t){
        
    }
    /**
     * 
     * @param t es la cadena a insertar en  la posición k del arreglo alumnos
     * @param k  es la posición de la cadena t en el arreglo alumnos
     * Aquí va más información
     */
    
    void setOCi(String t, int k){
        
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String[] md={"Ana","Elizabeth","Francisco","Hector"};
        OrdenamientoCadenas lmd=new OrdenamientoCadenas(md);
        System.out.println(lmd.pos("Gonzalo"));
    }
    
}

